view: sensor_data {
  sql_table_name: sensor_data ;;

#   dimension: dew_point {
#     type: number
#     sql: ${TABLE}.dew_point ;;
#   }

dimension: object_id {
  type: number
  hidden: yes
  primary_key: yes
  sql: ${TABLE}.object_id ;;
}

  dimension: humidity {
    description: "In Percentage"
    type: number
    sql: ${TABLE}.humidity ;;
  }

#   dimension: location {
#     type: string
#     sql: ${TABLE}.location ;;
#   }

  dimension: pressure {
    description: "In Pascal"
    type: number
    sql: round(${TABLE}.pressure*0.0002953, 1) ;;
  }

  dimension: temperature {
    description: "In Farenheight"
    type: number
    sql: ${TABLE}.temperature ;;
  }

  dimension: light {
    label: "light"
    description: "In Lux"
    type: number
    sql: ${TABLE}.light ;;
    #sql: ${TABLE}.dew_point ;;
  }

  dimension_group: ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.time_stamp ;;
    #convert_tz: no
  }

  measure: count {
    type: count
    drill_fields: []
  }
  measure: max_pressure {
    type: max
    sql: ${pressure} ;;
  }
  measure: min_pressure {
    type: min
    sql: ${pressure} ;;
  }
  measure: avg_pressure {
    type: average
    sql: ${pressure} ;;
  }
  measure: min_humidity {
    type: min
    sql: ${humidity} ;;
  }
  measure: max_humidity {
    type: max
    sql: ${humidity} ;;
  }
  measure: avg_humidity {
    type: average
    sql: ${humidity} ;;
  }

  measure: max_temperature {
    type: max
    sql: ${temperature} ;;
  }

dimension: temperature_gauge {
    sql: ${temperature} ;;
    html: <img src="https://chart.googleapis.com/chart?chs=250x250&cht=gom&chma=10,0,0,0&chxt=x,y&chco=0000FF,FF0000&chf=bg,s,FFFFFF00&chl={{ value }}&chd=t:{{ value }}&chxl=1:|cold|hot">;;
  }

  dimension: pressure_relative {
    sql: (${pressure}-26)/7*100 ;;
    #pressure ranges from 87000 to 108400
    #sql: (${pressure}-87000)/(108400-87000)*100 ;;
    #hidden: yes
  }

dimension: pressure_gauge {
  sql:  ${pressure};;
  #sea level is 29.92. (29.92)/7*100 -> 41.7
  #comment
  html: <img src="https://chart.googleapis.com/chart?chs=250x250&cht=gom&chma=10,0,0,0&chxt=x,y&chco=0000FF,FF0000&chf=bg,s,FFFFFF00&chl={{ value }}&chd=t:{{ pressure_relative._value }}|41.7&chxl=1:|low|high&chls=3|3,5,5|15|10">;;
}

dimension: light_relative {
  #wipedia says full daylight is 10,000 - 25000
  sql: ${light}/100 ;;
  #hidden: yes
}

dimension: light_gauge {
  sql:  ${light};;
  #html:  <img src="https://chart.googleapis.com/chart?chs=250x250&cht=gom&chma=10,0,0,0&chxt=x,y&chco=0000FF,FF0000&chf=bg,s,FFFFFF00&chl={{ value }}&chd=t:{{ light_relative._value }}&chxl=1:|office light|direct sunlight">;;
  html:  <img src="https://chart.googleapis.com/chart?chs=250x250&cht=gom&chma=10,0,0,0&chxt=x,y&chco=FFFF00,FF0000&chf=bg,s,FFFFFF00&chl={{ value }}&chd=t:{{ light_relative._value }}&chxl=1:|office light|direct sunlight">;;
}

dimension: humidity_gauge {
  sql: ${humidity} ;;
  html:  <img src="https://chart.googleapis.com/chart?chs=250x250&cht=gom&chma=10,0,0,0&chxt=x,y&chco=FF0000,0000FF,=&chf=bg,s,FFFFFF00&chl={{ value }}&chd=t:{{ value }}&chxl=1:|dry|wet">;;
}
dimension: ip_location {
  type: location
  sql_longitude: ${TABLE}.longitude ;;
  sql_latitude: ${TABLE}.latitude ;;
}
#Embedded Google map requires either iframe or javascript - neither of which can be done in html tag
dimension: google_map {
  type: string
  sql:  concat(${TABLE}.latitude, ',', ${TABLE}.longitude);;
  #html: <div style="text-decoration:none; overflow:hidden;max-width:100%;width:500px;height:500px;"><div id="embedmap-display" style="height:100%; width:100%;max-width:100%;"><iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=36.97400,+-122.02800&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8"></iframe></div><a class="embed-maphtml" rel="nofollow" href="https://changing.hosting" id="make-mapinformation">Changing Hosting</a><style>#embedmap-display img.text-marker{max-width:none!important;background:none!important;}img{max-width:none}</style></div>;;
  #html: <iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=36.97400,+-122.02800&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8"></iframe> ;;
  #html: <iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=36.97400,+-122.02800&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8"></iframe> ;;
  html: <a href="https://maps.google.com/maps?q=36.97400%2C-122.02800&t=&z=13&ie=UTF8"> ;;
  hidden:  yes
  }

}


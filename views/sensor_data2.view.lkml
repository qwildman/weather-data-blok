view: sensor_data2 {
  sql_table_name: sensor_data2 ;;

  dimension_group: ts {
    type: time
    label: "Timestamp"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ts ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
    suggestions: ["Humidity", "Temperature", "Pressure", "Light"]
  }

  dimension: value {
    type: number
    sql:case when ${type}='Temperature' then ${TABLE}.value*1.8 + 32 when ${type}='Humidity' then ${TABLE}.value*100 else ${TABLE}.value end;;
    hidden: yes
  }

  measure: count {
    type: count
    drill_fields: []
    hidden: yes
  }
  measure: max_value {
    type: max
    label: "value"
    sql: ${value} ;;
  }
  measure: temerature_gauge {
    type: max
    sql: ${value} ;;
    html: <img src="https://chart.googleapis.com/chart?chs=250x250&cht=gom&chma=10,0,0,0&chxt=x,y&chco=0000FF,FF0000&chf=bg,s,FFFFFF00&chl={{ value }}&chd=t:{{ value }}&chxl=1:|cold|hot">;;
  }
  measure: humidity_gauge {
    type: max
    sql: ${value} ;;
    html: <img src="https://chart.googleapis.com/chart?chs=250x250&cht=gom&chma=10,0,0,0&chxt=x,y&chco=FF0000,0000FF,=&chf=bg,s,FFFFFF00&chl={{ value }}&chd=t:{{ value }}&chxl=1:|dry|wet">;;
  }
  measure: pressure_relative {
   sql: (${pressure_gauge}-26)/6*100 ;;
  hidden: yes
  }
  measure: pressure_gauge {
    type: max
    sql: ${value} ;;
    html: <img src="https://chart.googleapis.com/chart?chs=250x250&cht=gom&chma=10,0,0,0&chxt=x,y&chco=0000FF,FF0000&chf=bg,s,FFFFFF00&chl={{ value }}&chd=t:{{ pressure_relative._value }}&chxl=1:|low|high">;;
  }
  measure: pressure_gauge_test {
    type: max
    sql: ${value} ;;
    html: <img src="https://chart.googleapis.com/chart?chs=250x250&cht=gom&chma=10,0,0,0&chco=0000FF,FF0000&chf=bg,s,FFFFFF00&chl={{ value }}&chd=t:{{ value }}&chxr=1,26,32&chxt=x,y">;;
  }
  measure: max_value2 {
    type: max
    label: "Max value"
    sql: ${value} ;;
  }
  measure: min_value {
    type: min
    sql: ${value} ;;
  }
  measure: avg_value {
    type: average
    sql: ${value} ;;
  }
}
